package com.sjhy.plugin.ui.base;

import com.intellij.util.ui.EditableModel;
import com.sjhy.plugin.entity.*;
import com.sjhy.plugin.enums.ColumnConfigType;
import com.sjhy.plugin.tool.CurrGroupUtils;

import javax.swing.table.DefaultTableModel;
import java.util.*;

/**
 * @author makejava
 * @version 1.0.0
 * @date 2021/08/14 13:41
 */
public class TableConfigModel extends DefaultTableModel implements EditableModel {

    private TableInfo tableInfo;

    public TableConfigModel(TableInfo tableInfo) {
        Map<String, Object> ext = tableInfo.getExt();
        if (ext == null) {
            ext = new HashMap<>();
            tableInfo.setExt(ext);
        }
        this.tableInfo = tableInfo;
        this.initColumn();
        this.initTableData();
    }

    private void initColumn() {
        addColumn("title");
        addColumn("value");
    }

    private void initTableData() {
        // 删除所有列
        int size = getRowCount();
        for (int i = 0; i < size; i++) {
            super.removeRow(0);
        }
        // 渲染列数据
        Map<String, Object> ext = this.tableInfo.getExt();
        for (TableConfig tableConfig : CurrGroupUtils.getCurrTableConfigGroup().getElementList()) {
            List<Object> values = new ArrayList<>();
            values.add(tableConfig.getTitle());
            Object obj = ext.get(tableConfig.getTitle());
            if (obj == null) {
                if (tableConfig.getType() == ColumnConfigType.BOOLEAN) {
                    values.add(false);
                } else {
                    values.add("");
                }
            } else {
                values.add(obj);
            }
            addRow(values.toArray());
        }
    }

    @Override
    public void addRow() {
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        List<TableConfig> elementList = CurrGroupUtils.getCurrTableConfigGroup().getElementList();
        TableConfig tableConfig = elementList.get(row);
        if (tableConfig == null) {
            return;
        }
        // 非自定义数据不允许修改
        if (column <= 0) {
            return;
        }
        Map<String, Object> ext = this.tableInfo.getExt();
        ext.put(tableConfig.getTitle(), value);
        super.setValueAt(value, row, column);
    }

    @Override
    public void removeRow(int row) {
    }

    @Override
    public void exchangeRows(int oldIndex, int newIndex) {
        ColumnInfo columnInfo = this.tableInfo.getFullColumn().remove(oldIndex);
        this.tableInfo.getFullColumn().add(newIndex, columnInfo);
        this.initTableData();
    }

    @Override
    public boolean canExchangeRows(int oldIndex, int newIndex) {
        return false;
    }
}
