package com.sjhy.plugin.ui;

import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.ui.ToolbarDecorator;
import com.intellij.ui.table.JBTable;
import com.sjhy.plugin.entity.TableInfo;
import com.sjhy.plugin.factory.CellEditorFactory;
import com.sjhy.plugin.service.TableInfoSettingsService;
import com.sjhy.plugin.tool.CacheDataUtils;
import com.sjhy.plugin.tool.ProjectUtils;
import com.sjhy.plugin.ui.base.TableConfigModel;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.*;

/**
 * 表配置窗口
 *
 * @author makejava
 * @version 1.0.0
 * @since 2018/07/17 13:10
 */
public class TableConfigDialog extends DialogWrapper {
    /**
     * 主面板
     */
    private final JPanel mainPanel;
    /**
     * 表信息对象
     */
    private TableInfo tableInfo;

    public TableConfigDialog() {
        super(ProjectUtils.getCurrProject());
        this.mainPanel = new JPanel(new BorderLayout());
        this.initPanel();
    }

    private void initPanel() {
        init();
        this.tableInfo = TableInfoSettingsService.getInstance().getTableInfo(CacheDataUtils.getInstance().getSelectDbTable());
        setTitle("Config Table " + this.tableInfo.getObj().getName());
        TableConfigModel tableExtModel = new TableConfigModel(this.tableInfo);
        JBTable table = new JBTable(tableExtModel);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        int totalWidth = 0;

        // 配置列编辑器
        TableColumn nameColumn = table.getColumn("title");
        nameColumn.setCellEditor(CellEditorFactory.createTextFieldEditor());
        nameColumn.setMinWidth(100);
        totalWidth+=100;
        TableColumn typeColumn = table.getColumn("value");
        typeColumn.setCellEditor(CellEditorFactory.createTextFieldEditor());
//        typeColumn.setCellRenderer(new ComboBoxTableRenderer<>(GlobalDict.DEFAULT_JAVA_TYPE_LIST));
//        typeColumn.setCellEditor(CellEditorFactory.createComboBoxEditor(true, GlobalDict.DEFAULT_JAVA_TYPE_LIST));
        typeColumn.setMinWidth(120);
        totalWidth+=120;

        final ToolbarDecorator decorator = ToolbarDecorator.createDecorator(table);
        this.mainPanel.add(decorator.createPanel(), BorderLayout.CENTER);
        this.mainPanel.setMinimumSize(new Dimension(totalWidth, Math.max(300, totalWidth / 3)));
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return this.mainPanel;
    }

    @Override
    protected void doOKAction() {
        // 保存信息
        TableInfoSettingsService.getInstance().saveTableInfo(tableInfo);
        super.doOKAction();
    }

}
